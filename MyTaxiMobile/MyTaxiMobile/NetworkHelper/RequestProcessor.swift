//
//  requestProcessor.swift
//  MyTaxiMobile
//
//  Created by IOS Developer on 4/17/18.
//  Copyright © 2018 Ehioze Iweka. All rights reserved.
//

import UIKit

@objc class RequestProcessor: NSObject {
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    var theerrorvalue = ""

   @objc func retrirveVehiclesBasedonBoundries(_ firstPointLat:Double, _ firstPointLong:Double, _ secondPointLat:Double, _ secondPointLong:Double, completionHandler:@escaping ((_ vehicles_retrieved : [MyTaxiVehicle]?, _ errorMessage:String?) -> Void)){
        dataTask?.cancel()
        let theUrlValue = mytaxis_In_hamburg.replacingOccurrences(of: "{p1lon}", with: String.init(format: "%.6f", firstPointLong)).replacingOccurrences(of: "{p2lat}", with: String.init(format: "%.6f", secondPointLat)).replacingOccurrences(of: "{p1lat}", with: String.init(format: "%.6f", firstPointLat)).replacingOccurrences(of: "{p2lon}", with: String.init(format: "%.6f", secondPointLong))
        guard let url = URL(string:theUrlValue) else {
            return }
        dataTask = defaultSession.dataTask(with: url) { data, response, error in
            defer { self.dataTask = nil }
            if let error = error
            {
                self.theerrorvalue += "DataTask error: " + error.localizedDescription + "\n"
            }
            else if let data = data
            {
                guard let vehiclesReturned = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)) else {
                    completionHandler(nil, "Error performing request")
                    return
                }
                print("these is the response ohhh \(vehiclesReturned)")
                if let vehicleObject = vehiclesReturned as? [String: Any]{
                    let returnedVehicles = vehicleObject["poiList"] as! [[String:Any]]
                    var vehicleCollection:[MyTaxiVehicle] = [MyTaxiVehicle]()
                    for vehicleItems in returnedVehicles{
                        let theTaxiVehicles = MyTaxiVehicle(vehicleItems)
                        vehicleCollection.append(theTaxiVehicles)
                    }
                    completionHandler(vehicleCollection, nil)
                }
                else{
                    completionHandler(nil, "Error retrieving categories")
                }
            }
        }
        dataTask?.resume()
    }
    
    func retrirveDistanceMatrixForLocations(_ firstPointLat:String, _ firstPointLong:String, _ secondPointLat:String, _ secondPointLong:String, completionHandler:@escaping ((_ distance_retrieved:[String:Any], _ errorMessage:String?) -> Void)){
        var distanceValue = [String: Any]()
        dataTask?.cancel()
        let theUrlValue = distanceMatrixAPI.replacingOccurrences(of: "{startlat}", with:firstPointLat).replacingOccurrences(of: "{startlong}", with:firstPointLong).replacingOccurrences(of: "{endlat}", with:secondPointLat).replacingOccurrences(of: "{endlong}", with: secondPointLong).replacingOccurrences(of: "{key}", with: googleMapAPIKey)
        guard let url = URL(string:theUrlValue) else {
            return }
        dataTask = defaultSession.dataTask(with: url) { data, response, error in
            defer { self.dataTask = nil }
            if let error = error{
                self.theerrorvalue += "DataTask error: " + error.localizedDescription + "\n"
            }
            else if let data = data{
                guard let distanceReturned = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers))  else {
                    completionHandler(distanceValue, "Error performing request")
                    return
                }
                if let valueReturned = distanceReturned as? [String:Any] {
                    let modeledValue = DistanceMAtrix(valueReturned)
                    distanceValue.updateValue(modeledValue, forKey: "distanceInformation")
                  completionHandler(distanceValue, nil)
                }
                else{
                    completionHandler(distanceValue, "Error retrieving categories")
                }
            }
        }
        dataTask?.resume()
    }
}
