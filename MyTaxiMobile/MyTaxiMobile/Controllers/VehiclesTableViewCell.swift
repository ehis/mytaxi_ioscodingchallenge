//
//  VehiclesTableViewCell.swift
//  MyTaxiMobile
//
//  Created by IOS Developer on 4/17/18.
//  Copyright © 2018 Ehioze Iweka. All rights reserved.
//

import UIKit

/**protocol VehicleSelectionOperation:class {
    func vehicleSelected(categoryItem:Int)
}**/

class VehiclesTableViewCell: UITableViewCell {
    @IBOutlet weak var theVehicleImage: UIImageView!
    @IBOutlet weak var theVehicleId: UILabel!
    @IBOutlet weak var theVehicleLatandLong: UILabel!
    @IBOutlet weak var theVehicleDirection: UILabel!
    @IBOutlet weak var theVehicleType: UILabel!
    @IBOutlet weak var theVehicleState: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpValues(_ theSelectedVehicle:MyTaxiVehicle)
    {
        if theSelectedVehicle.vehicleState == "ACTIVE"{
            theVehicleImage.image = UIImage.init(named: "taxi_active")
            theVehicleState.textColor = UIColor.green
            theVehicleState.text = theSelectedVehicle.vehicleState
        }
        else
        {
            theVehicleImage.image = UIImage.init(named: "taxi_inactive")
            theVehicleState.textColor = UIColor.lightGray
            theVehicleState.text = theSelectedVehicle.vehicleState
        }
        theVehicleId.text=String.init(format: "%d", theSelectedVehicle.vehicleID)
        theVehicleDirection.text=String.init(format: "Heading: %.2f", theSelectedVehicle.vehicleHeading)
        theVehicleType.text = theSelectedVehicle.vehicleType
        theVehicleLatandLong.text=String.init(format: "LAT:%.2f LONG:%.2f", theSelectedVehicle.vehicleLatitude, theSelectedVehicle.vehicleLongitude)
       // let rotation = CGFloat(theSelectedVehicle.vehicleHeading/180 * Double.pi)
        //theVehicleImage.transform = CGAffineTransform(rotationAngle: rotation)
    }


}
