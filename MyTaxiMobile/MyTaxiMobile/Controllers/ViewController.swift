//
//  ViewController.swift
//  MyTaxiMobile
//
//  Created by IOS Developer on 4/14/18.
//  Copyright © 2018 Ehioze Iweka. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    @IBOutlet weak var theIndicator: UIActivityIndicatorView!
    @IBOutlet weak var theVehiclesTable: UITableView!
    @IBOutlet weak var vehicleDetails: UIView!
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var vehicleState: UILabel!
    @IBOutlet weak var vehicleLocation: UILabel!
    @IBOutlet weak var vehicleDistanceApart: UILabel!
    @IBOutlet weak var vehicleEstimatedTimeOfArrival: UILabel!
    
    let cellIdentifier = "vehicleCell"
    var vehicleCollection = [MyTaxiVehicle]()
    var currentLatitude:String?
    var currentLongitude:String?
    var locationManager:CLLocationManager?
    var vehicleSelected:MyTaxiVehicle?
    override func viewDidLoad() {
        super.viewDidLoad()
        retrieveVehiclesInHamburg()
        initialiseLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func initialiseLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate=self
        locationManager?.desiredAccuracy=kCLLocationAccuracyBest
        locationManager?.distanceFilter=kCLDistanceFilterNone
        locationManager?.requestWhenInUseAuthorization()
        
        locationManager?.startUpdatingLocation()
    }
    
    
    
    func retrieveVehiclesInHamburg() {
        theIndicator.startAnimating()
        RequestProcessor().retrirveVehiclesBasedonBoundries(53.694865,9.757589,53.394655,10.099891){(theVehiclesReturned,theErrorMessage) in
            guard let theVehicleItem = theVehiclesReturned else {
                print("error getting file ohhhh")
                DispatchQueue.main.async {
                self.showSimpleAlert("Request Failed", message:theErrorMessage)
                self.theIndicator.stopAnimating()
                }
                return
            }
            self.vehicleCollection = theVehicleItem
            DispatchQueue.main.async {
                self.theIndicator.stopAnimating()
                self.theVehiclesTable.reloadData()
            }
            
        }
    }
    
    func getDistanceInformationForSelectedVehicle(_ theFirstLat:String, _ theFirstLong:String, _ theSecondLat:String, _ theSecondLong:String){
        theIndicator.startAnimating()
        RequestProcessor().retrirveDistanceMatrixForLocations(theFirstLat,theFirstLong,theSecondLat,theSecondLong){(theDistanceReturned,theErrorMessage) in
            if theErrorMessage==nil{
                let theValueReturned = theDistanceReturned["distanceInformation"] as! DistanceMAtrix
                DispatchQueue.main.async {
                    self.theIndicator.stopAnimating()
                    self.vehicleDetails.isHidden = false
                    if self.vehicleSelected!.vehicleState == "ACTIVE"{
                        self.vehicleImage.image = UIImage.init(named: "taxi_active")
                        self.vehicleState.textColor = UIColor.green
                        self.vehicleState.text = self.vehicleSelected!.vehicleState
                    }
                    else{
                        self.vehicleImage.image = UIImage.init(named: "taxi_inactive")
                        self.vehicleState.textColor  = UIColor.lightGray
                        self.vehicleState.text = self.vehicleSelected!.vehicleState
                    }
                    self.vehicleLocation.text = theValueReturned.originAddress
                    self.vehicleEstimatedTimeOfArrival.text = theValueReturned.duration
                    self.vehicleDistanceApart.text = theValueReturned.distance?.replacingOccurrences(of: "mi", with: "metres")
                }
            }
            else{
                DispatchQueue.main.async {
                    self.showSimpleAlert("Request Failed", message:theErrorMessage)
                    self.theIndicator.stopAnimating()
                }
            }
    }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        locationManager?.stopUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        locationManager?.stopUpdatingLocation()
    }

    @IBAction func displayMapView(_ sender: UIButton) {
        self.performSegue(withIdentifier: SEGUE_TO_DISPLAY_MAP, sender: self)
    }
    @IBAction func dismissDetailsView(_ sender: UIButton) {
        self.vehicleDetails.isHidden=true
    }
    
}

extension ViewController:UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vehicleCollection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! VehiclesTableViewCell
        let theVehicleItem = self.vehicleCollection[indexPath.row]
        cell.setUpValues(theVehicleItem)
        return cell
}
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        vehicleSelected = self.vehicleCollection[indexPath.row]
        getDistanceInformationForSelectedVehicle(String.init(format: "%.6f",vehicleSelected!.vehicleLatitude),String.init(format: "%.6f",vehicleSelected!.vehicleLongitude),currentLatitude!,currentLongitude!)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let theCurrentLocation = locations.last!
        currentLatitude = String.init(format: "%f", theCurrentLocation.coordinate.latitude)
        currentLongitude = String.init(format: "%f", theCurrentLocation.coordinate.longitude)
        
        print("my location has been updated ohhhhhh +++++++ ++++++ +++++++++ ============((((()))) &**&*^*()))))))))))))))))))))))))))))))))) \n telll them again aohhhhhhh kkkkek")
    }
    
}

