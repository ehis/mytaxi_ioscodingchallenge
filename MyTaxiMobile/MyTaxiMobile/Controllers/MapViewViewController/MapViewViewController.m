//
//  MapViewViewController.m
//  MyTaxiMobile
//
//  Created by IOS Developer on 4/18/18.
//  Copyright © 2018 Ehioze Iweka. All rights reserved.
//
#import "MyTaxiMobile-Swift.h"
#import "MapViewViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewViewController ()<CLLocationManagerDelegate,GMSMapViewDelegate>{
    NSString *strForCurrentLatitude,*strForCurrentLongitude;
    MyTaxiVehicle *theVehicleDetails;
    CLLocationManager *locationManager;
}
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *theIndicator;

@end

@implementation MapViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    [self updateLocationManager];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [locationManager stopUpdatingHeading];
}

-(void)displayVehiclesOnTheMap{
    [_mapView clear];
     for (theVehicleDetails in self.theVehiclesReturned) {
         GMSMarker *vehicle_marker = [[GMSMarker alloc] init];
         vehicle_marker.position = CLLocationCoordinate2DMake(theVehicleDetails.vehicleLatitude,theVehicleDetails.vehicleLongitude);
         CGFloat rotation = (theVehicleDetails.vehicleHeading/180 * M_PI);
         vehicle_marker.title = [NSString stringWithFormat:@"%ld",(long)theVehicleDetails.vehicleID];
         UIView *theIconView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
         theIconView.backgroundColor = [UIColor clearColor];
         UIImageView *theImageView = [[UIImageView alloc] initWithFrame:theIconView.frame];
         theImageView.image = [UIImage imageNamed:([theVehicleDetails.vehicleState isEqualToString:@"ACTIVE"]?@"taxi_active_reduced":@"taxi_inactive_reduced")];
         theImageView.transform = CGAffineTransformMakeRotation(rotation);
         [theIconView addSubview:theImageView];
         [vehicle_marker.iconView sizeToFit];
         vehicle_marker.iconView = theIconView;
         vehicle_marker.map = _mapView;
     }
}

-(void)showUserLocation{
    CLLocationCoordinate2D coor;
    coor.latitude=[strForCurrentLatitude doubleValue];
    coor.longitude=[strForCurrentLongitude doubleValue];
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:12];
    [_mapView animateWithCameraUpdate:updatedCamera];
}

- (IBAction)performBackOperation:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)updateLocationManager{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    if([[[UIDevice currentDevice] systemVersion] compare:@"8" options:NSNumericSearch] != NSOrderedAscending) {
        if([[[UIDevice currentDevice] systemVersion] compare:@"11" options:NSNumericSearch] != NSOrderedAscending) {
            [locationManager requestWhenInUseAuthorization];
            }
        else{
            [locationManager requestAlwaysAuthorization];
        }
    }
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    strForCurrentLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    strForCurrentLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    [self showUserLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError: %@", error);
}



#pragma mark- Google Map Delegate

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    strForCurrentLatitude=[NSString stringWithFormat:@"%f",position.target.latitude];
    strForCurrentLongitude=[NSString stringWithFormat:@"%f",position.target.longitude];
}

- (void) mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    GMSVisibleRegion visibleRegion = [mapView.projection visibleRegion];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc]initWithRegion:visibleRegion];
    CLLocationCoordinate2D FirstPoint = bounds.northEast;
    CLLocationCoordinate2D secondPoint = bounds.southWest;
    [self getVehiclesForSelectedBounds:FirstPoint theSecondPoint:secondPoint];
}

-(void)getVehiclesForSelectedBounds:(CLLocationCoordinate2D)firstPoint theSecondPoint:(CLLocationCoordinate2D)secondPoint{
    [_theIndicator startAnimating];
    RequestProcessor *theRequestProcessor = [[RequestProcessor alloc] init];
    [theRequestProcessor retrirveVehiclesBasedonBoundries:firstPoint.latitude :firstPoint.longitude :secondPoint.latitude :secondPoint.longitude completionHandler:^(NSArray *theReturnedVehicles, NSString *theError){
        if(theError==nil){
        self.theVehiclesReturned = theReturnedVehicles;
            dispatch_async(dispatch_get_main_queue(), ^{
                [_theIndicator stopAnimating];
                [self displayVehiclesOnTheMap];
            });
        }
        else{
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Error Retrieving Vehicles"
                                                  message:theError
                                                  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action){
                                       }];
            [alertController addAction:okAction];
            dispatch_async(dispatch_get_main_queue(), ^{
                [_theIndicator stopAnimating];
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }
    }];
}



@end
