//
//  UtilityExtensions.swift
//  MyTaxiMobile
//
//  Created by IOS Developer on 4/17/18.
//  Copyright © 2018 Ehioze Iweka. All rights reserved.

import UIKit

extension UIViewController
{
    /**
     A utility function that displays a UIAlert for any UIView Controller.
     
     ## Important Notes ##
     1. This function can be accessed form any UIViewController subclass
     2. It accepts two parameters which are title and message.
     
     - Parameter title: The title of the alert. This is passed as the title parameter of the UIAlert.
     - Parameter message: The message of the alert. This is passed as the message parameter of the UIAlert
     
 */
     func showSimpleAlert(_ title:String, message:String?) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler:nil)
        alertView.addAction(okAction)
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.windowLevel = UIWindowLevelAlert
        alertWindow.rootViewController = UIViewController()
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertView, animated: true, completion: nil)
    }
    
}

extension UIButton
{
    /// An IBInspectable variable for UIView and its subclasses, that allows for the modification of the corner radius property of the UIView or its subclasses.
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    /// An IBInspectable variable for UIView and its subclasses, that allows for the modification of the border width property of the UIView or its subclasses.
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    /// An IBInspectable variable for UIView and its subclasses, that allows for the modification of the border color property of the UIView or its subclasses.
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
}






