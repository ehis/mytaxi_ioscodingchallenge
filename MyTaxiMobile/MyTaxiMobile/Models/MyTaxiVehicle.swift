//
//  MyTaxiVehicle.swift
//  MyTaxiMobile
//
//  Created by IOS Developer on 4/17/18.
//  Copyright © 2018 Ehioze Iweka. All rights reserved.
//

import Foundation

@objcMembers
  class MyTaxiVehicle :NSObject{
    var vehicleID=0
    var vehicleLatitude=0.00
    var vehicleLongitude=0.00
    var vehicleState:String?
    var vehicleType:String?
    var vehicleHeading=0.00
    
    init(_ theVehicleDetails:[String:Any]) {
        if let theVehicleId = theVehicleDetails["id"] as? Int{
            self.vehicleID = theVehicleId
        }
        
        if let theVehicleLat = (theVehicleDetails["coordinate"] as! [String:Any])["latitude"] as? Double{
            self.vehicleLatitude = theVehicleLat
        }
        if let theVehicleLon = (theVehicleDetails["coordinate"] as! [String:Any])["longitude"] as? Double{
            self.vehicleLongitude = theVehicleLon
        }
        if let theVehicleState = theVehicleDetails["state"] as? String{
            self.vehicleState = theVehicleState
        }
        if let theVehicleType = theVehicleDetails["type"] as? String{
            self.vehicleType = theVehicleType
        }
        if let theVehicledirection = theVehicleDetails["heading"] as? Double{
            self.vehicleHeading = theVehicledirection
        }
    }
}
