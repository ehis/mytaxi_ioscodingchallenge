//
//  DistanceMatrix.swift
//  MyTaxiMobile
//
//  Created by IOS Developer on 4/19/18.
//  Copyright © 2018 Ehioze Iweka. All rights reserved.
//

import Foundation

struct DistanceMAtrix {
    var destinationAddress:String?
    var originAddress:String?
    var distance:String?
    var duration:String?
    
    init(_ theDiatanceDetails:[String:Any]) {
        let thedstAddressArray = theDiatanceDetails["destination_addresses"] as! [String]
        let theoriginAddressArray = theDiatanceDetails["origin_addresses"] as! [String]
        if let theDestAddress = thedstAddressArray.last{
            destinationAddress = theDestAddress
        }
        if let theOriginAddress = theoriginAddressArray.last{
            originAddress = theOriginAddress
        }
        let theDetails = theDiatanceDetails["rows"] as! [[String:Any]]
        if let theElements = theDetails.first{
        let theElementsArray = theElements["elements"] as! [[String:Any]]
        let theElementsDictionary = theElementsArray.first
        distance = (theElementsDictionary!["distance"] as! [String:Any])["text"] as? String
        duration = (theElementsDictionary!["duration"] as! [String:Any])["text"] as? String
        }
    }
    
}
